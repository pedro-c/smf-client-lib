package com.smf.smfclientlibwithmethods.model;

import graphql.language.StringValue;
import graphql.schema.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author bogdankobylinsky
 */
@Configuration
public class GraphQLConfiguration {

    private static final DateTimeFormatter ZONED_DATE_TIME_FORMAT = DateTimeFormatter.ISO_ZONED_DATE_TIME;

    @Bean
    public GraphQLScalarType zonedDateTimeGraphQLScalarType() {
        return GraphQLScalarType.newScalar()
                .name("ZonedDateTime")
                .coercing(new Coercing() {
                    @Override
                    public Object serialize(Object o) throws CoercingSerializeException {
                        return ZONED_DATE_TIME_FORMAT.format((ZonedDateTime) o);
                    }

                    @Override
                    public Object parseValue(Object o) throws CoercingParseValueException {
                        return serialize(o);
                    }

                    @Override
                    public Object parseLiteral(Object o) throws CoercingParseLiteralException {
                        return ZONED_DATE_TIME_FORMAT.parse(((StringValue) o).getValue());
                    }
                }).build();
    }

}
