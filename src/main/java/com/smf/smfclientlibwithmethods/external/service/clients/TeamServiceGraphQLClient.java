package com.smf.smfclientlibwithmethods.external.service.clients;

import com.kobylynskyi.graphql.codegen.model.graphql.GraphQLRequest;
import com.kobylynskyi.graphql.codegen.model.graphql.GraphQLResult;
import com.smf.smfclientlibwithmethods.external.util.HttpClientUtil;
import com.smf.smfclientlibwithmethods.model.BasketballTeam;
import com.smf.smfclientlibwithmethods.model.BasketballTeamResponseProjection;
import com.smf.smfclientlibwithmethods.model.TeamQueryRequest;
import com.smf.smfclientlibwithmethods.model.TeamsQueryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

@Service
public class TeamServiceGraphQLClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${external.service.url}")
    private String url;

    public BasketballTeam getTeamById(String teamId) throws Exception {
        TeamQueryRequest teamRequest = new TeamQueryRequest();
        teamRequest.setTeamId(teamId);
        GraphQLRequest request = new GraphQLRequest(teamRequest,
                new BasketballTeamResponseProjection()
                        .id()
                        .name()
                        .type());
        GraphQLResult<Map<String, BasketballTeam>> result = restTemplate.exchange(URI.create(url),
                HttpMethod.POST,
                HttpClientUtil.httpEntity(request),
                new ParameterizedTypeReference<GraphQLResult<Map<String, BasketballTeam>>>() {})
                .getBody();
        if (result.hasErrors()) {
            throw new Exception(result.getErrors().get(0).getMessage());
        }
        return result.getData().get(teamRequest.getOperationName());
    }

    public Collection<BasketballTeam> getAllTeams() throws Exception {
        TeamsQueryRequest teamsRequest = new TeamsQueryRequest();
        GraphQLRequest request = new GraphQLRequest(teamsRequest,
                new BasketballTeamResponseProjection()
                        .id()
                        .name()
                        .type());
        GraphQLResult<Map<String, Collection<BasketballTeam>>> result = restTemplate.exchange(URI.create(url),
                HttpMethod.POST,
                HttpClientUtil.httpEntity(request),
                new ParameterizedTypeReference<GraphQLResult<Map<String, Collection<BasketballTeam>>>>() {})
                .getBody();
        if (result.hasErrors()) {
            throw new Exception(result.getErrors().get(0).getMessage());
        }
        return result.getData().get(teamsRequest.getOperationName());
    }

}
