package com.smf.smfclientlibwithmethods.external.service.clients;

import com.kobylynskyi.graphql.codegen.model.graphql.GraphQLRequest;
import com.kobylynskyi.graphql.codegen.model.graphql.GraphQLResult;
import com.smf.smfclientlibwithmethods.external.util.HttpClientUtil;
import com.smf.smfclientlibwithmethods.model.BasketballCompetition;
import com.smf.smfclientlibwithmethods.model.BasketballCompetitionResponseProjection;
import com.smf.smfclientlibwithmethods.model.CompetitionQueryRequest;
import com.smf.smfclientlibwithmethods.model.CompetitionsQueryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

@Service
public class CompetitionServiceGraphQLClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${external.service.url}")
    private String url;

    public BasketballCompetition getCompetitionById(String competitionId) throws Exception {
        CompetitionQueryRequest getCompetitionRequest = new CompetitionQueryRequest();
        getCompetitionRequest.setCompetitionId(competitionId);
        GraphQLRequest request = new GraphQLRequest(getCompetitionRequest,
                new BasketballCompetitionResponseProjection()
                        .id()
                        .name());
        GraphQLResult<Map<String, BasketballCompetition>> result = restTemplate.exchange(URI.create(url),
                HttpMethod.POST,
                HttpClientUtil.httpEntity(request),
                new ParameterizedTypeReference<GraphQLResult<Map<String, BasketballCompetition>>>() {})
                .getBody();
        if (result.hasErrors()) {
            throw new Exception(result.getErrors().get(0).getMessage());
        }
        return result.getData().get(getCompetitionRequest.getOperationName());
    }

    public Collection<BasketballCompetition> getAllCompetitions() throws Exception {
        CompetitionsQueryRequest getCompetitionsRequest = new CompetitionsQueryRequest();
        GraphQLRequest request = new GraphQLRequest(getCompetitionsRequest,
                new BasketballCompetitionResponseProjection()
                        .id()
                        .name());
        GraphQLResult<Map<String, Collection<BasketballCompetition>>> result = restTemplate.exchange(URI.create(url),
                HttpMethod.POST,
                HttpClientUtil.httpEntity(request),
                new ParameterizedTypeReference<GraphQLResult<Map<String, Collection<BasketballCompetition>>>>() {})
                .getBody();
        if (result.hasErrors()) {
            throw new Exception(result.getErrors().get(0).getMessage());
        }
        return result.getData().get(getCompetitionsRequest.getOperationName());
    }

}
