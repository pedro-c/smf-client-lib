package com.smf.smfclientlibwithmethods.external.service.clients;

import com.kobylynskyi.graphql.codegen.model.graphql.GraphQLRequest;
import com.kobylynskyi.graphql.codegen.model.graphql.GraphQLResult;
import com.smf.smfclientlibwithmethods.external.util.HttpClientUtil;
import com.smf.smfclientlibwithmethods.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

@Service
public class GameServiceGraphQLClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${external.service.url}")
    private String url;

    public BasketballGame getBasketballGameById(String gameId) throws Exception {
        GameQueryRequest gameRequest = new GameQueryRequest();
        gameRequest.setGameId(gameId);
        GraphQLRequest request = new GraphQLRequest(gameRequest,
                new BasketballGameResponseProjection()
                        .id()
                        .teams(new BasketballTeamResponseProjection()
                                .id()
                                .name()
                                .type())
                        .competition(new BasketballCompetitionResponseProjection()
                                .id()
                                .name())
                        .venue()
                        .startTime());
        GraphQLResult<Map<String, BasketballGame>> result = restTemplate.exchange(URI.create(url),
                HttpMethod.POST,
                HttpClientUtil.httpEntity(request),
                new ParameterizedTypeReference<GraphQLResult<Map<String, BasketballGame>>>() {})
                .getBody();
        if (result.hasErrors()) {
            throw new Exception(result.getErrors().get(0).getMessage());
        }
        return result.getData().get(gameRequest.getOperationName());
    }

    public Collection<BasketballGame> getAllCBasketballGames() throws Exception {
        GamesQueryRequest gamesRequest = new GamesQueryRequest();
        GraphQLRequest request = new GraphQLRequest(gamesRequest,
                new BasketballGameResponseProjection()
                        .id()
                        .teams(new BasketballTeamResponseProjection()
                                .id()
                                .name()
                                .type())
                        .competition(new BasketballCompetitionResponseProjection()
                                .id()
                                .name())
                        .venue()
                        .startTime());
        GraphQLResult<Map<String, Collection<BasketballGame>>> result = restTemplate.exchange(URI.create(url),
                HttpMethod.POST,
                HttpClientUtil.httpEntity(request),
                new ParameterizedTypeReference<GraphQLResult<Map<String, Collection<BasketballGame>>>>() {})
                .getBody();
        if (result.hasErrors()) {
            throw new Exception(result.getErrors().get(0).getMessage());
        }
        return result.getData().get(gamesRequest.getOperationName());
    }

}
