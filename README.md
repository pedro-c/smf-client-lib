# smf-client-lib

* The purpose of this project is make a proof of concept of client library that will make queries to the Entity API.
* All the queries are present in this library.
* It is dependent on the graphql schema 
    * If the shcema changes there is no need to create the models, they are generaed from shcma
    * Need to adapt the queries to the new schama
    * Can fetch only the data that it needs
 * There are almost non code to be created for the new queries
    * Should refactor the queries to remove duplicated code between then
 
 * This query will be used by the SMF BackEnd or other applications that need to consume from Sports Entities API 
```
​
+-----------+            +-----------+          +----------+
|    SMF    +----------->+    SMF    +--------->+   SMF    |
|    UI     |            |  BackEnd  |          | Mock API |
|           +<-----------+           +<---------+          |
+-----------+            +-----------+          +----------+

```

## Main Features
* Query games, Competitions and Teams

# Getting Started

## Requirements
* JDK 11
* Maven

## How to used
* Generate resources by exexuting ```mvn generate-sources```
* Build smf-client-lib jar ```mvn clean install```
* Add dependency to your project

### Reference Documentation
The following guides illustrate the API that is being mocked and the diagrams:

* [Entity Query Service Interface](https://sportsbet.atlassian.net/wiki/spaces/ALIUM/pages/469270883/3.7.1.2.1+Entity+Query+Service+Interface+DRAFT)
* [Create Game](https://sportsbet.atlassian.net/wiki/spaces/ALIUM/pages/495027253/Create+Game)
* [Scalar for Data time](https://github.com/donbeave/graphql-java-datetime)
* [GraphQL Java Generator](https://github.com/graphql-java-generator/graphql-maven-plugin-project)
* [GraphQL Codegen Maven plugin](https://github.com/kobylynskyi/graphql-java-codegen/tree/master/plugins/maven)
